# GZIP decoder

A simple gzip decoder. The aim was to get a basic introduction to the world of compression.   
This is an educational project and is therefore not suitable to be used in other projects.  
Built in C and supports Linux and Windows.  

### Building

For Windows run the **build_win32.bat** file:
```
build_win32.bat
```
For Linux run the **build_linux.sh** file:
```
build_linux.sh
```
Both build files uses Clang to build so Clang needs to be available. 
