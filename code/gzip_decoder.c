#if defined(_MSC_VER)
#pragma warning(push, 0)
#elif defined(__clang__)
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Weverything"
#else
#error "Unsupported Compiler Detected"
#endif

#if defined(__linux__)
#include <fcntl.h>
#include <unistd.h>
#include <sys/stat.h>

typedef struct stat file_stat;

#elif defined(_WIN64)
#define WIN32_LEAN_AND_MEAN
#include <Windows.h>
#else
#error "Unsupported OS Detected"
#endif

#include <limits.h>
#include <stddef.h>
#include <memory.h>
#include <stdbool.h>
#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <assert.h>

#if defined(_MSC_VER)
#pragma warning(pop)
#elif defined(__clang__)
#pragma GCC diagnostic pop
#endif

#define tbs_assert(cond) assert(cond)
#if defined(__clang__)
#define tbs_static_assert(cond, message) _Static_assert(cond, message)
#elif defined(_MSC_VER)
#define tbs_static_assert(cond, message) _STATIC_ASSERT(cond);
#else
#error "Unsupported compiler detected"
#endif

#define ARRAY_COUNT(arr) (sizeof(arr) / sizeof(arr[0]))

#if TBS_DEBUG
#if defined(__clang__)
__attribute__((__format__(__printf__, 1, 2)))
#endif
static void PrintDebugF(char *text, ...)
{
    va_list argList;
    va_start(argList, text);
    vprintf(text, argList);
    va_end(argList);
}
#else
#define PrintDebugF(text, ...)
#endif

#if defined(__clang__)
__attribute__((__format__(__printf__, 1, 2)))
#endif
static void PrintTextF(char *text, ...)
{
    va_list argList;
    va_start(argList, text);
    vprintf(text, argList);
    va_end(argList);
}

typedef uint8_t u8;
typedef int8_t i8;
typedef uint16_t u16;
typedef int16_t i16;
typedef uint32_t u32;
typedef int32_t i32;
typedef uint64_t u64;
typedef int64_t i64;

typedef enum debug_timer_type
{
    DebugTimerType_GlobalApplication,
    DebugTimerType_FixedHuffman,
    DebugTimerType_DynamicHuffman,
    DebugTimerType_NonCompressed,
    DebugTimerType_CRC32,
    DebugTimerType_Count,
} debug_timer_type;

static i64 Debug_timerList[DebugTimerType_Count];

#if defined(_WIN64)

static inline i64 GetCurrentTicks(void)
{
    LARGE_INTEGER result;
    QueryPerformanceCounter(&result);
    return result.QuadPart;
}

static inline i64 GetClockFrequency(void)
{
    LARGE_INTEGER result;
    QueryPerformanceFrequency(&result);
    return result.QuadPart;
}

#define Debug_StartTimer(ID) i64 Debug_startTimer##ID = GetCurrentTicks()
#define Debug_EndTimer(ID) i64 Debug_endTimer##ID = (GetCurrentTicks() - Debug_startTimer##ID); Debug_timerList[DebugTimerType_##ID] += (Debug_endTimer##ID * 1000000) / GetClockFrequency()

#elif defined(__linux__)

#define Debug_StartTimer(ID)
#define Debug_EndTimer(ID)

#endif

typedef enum gzip_flags
{
    GzipFlags_FTEXT     = 1 << 0,
    GzipFlags_FHCRC     = 1 << 1,
    GzipFlags_FEXTRA    = 1 << 2,
    GzipFlags_FNAME     = 1 << 3,
    GzipFlags_FCOMMENT  = 1 << 4,
} gzip_flags;

typedef struct file_data
{
    u8 *data;
    u64 size;
} file_data;

typedef struct byte_stream
{
    u8 *base;
    u8 *offset;
    u64 size;
} byte_stream;

typedef struct bit_stream
{
    u8 *base;
    u8 *offset;
    u32 bitOffset;
    u64 size;
} bit_stream;

#pragma pack(push, 1)
typedef struct gzip_header
{
    u8 ID1;
    u8 ID2;
    u8 CM;
    u8 FLG;
    u32 MTIME;
    u8 XFL;
    u8 OS;
} gzip_header;
#pragma pack(pop)

typedef enum block_type
{
    BlockType_NoCompress,
    BlockType_Fixed,
    BlockType_Dynamic,
    BlockType_Reserved,
} block_type;

typedef struct code_lookup
{
    u32 baseValue;
    u32 extraBits;
} code_lookup;

typedef enum node_type
{
    NodeType_None,
    NodeType_Root,
    NodeType_Leaf,
    NodeType_Bit,
} node_type;

typedef struct huff_tree_node
{
    node_type type;
    struct huff_tree_node *children[2];
    u32 value;
} huff_tree_node;

static code_lookup lengthLookup[] =
{
    {.baseValue = 3,   .extraBits = 0},
    {.baseValue = 4,   .extraBits = 0},
    {.baseValue = 5,   .extraBits = 0},
    {.baseValue = 6,   .extraBits = 0},
    {.baseValue = 7,   .extraBits = 0},
    {.baseValue = 8,   .extraBits = 0},
    {.baseValue = 9,   .extraBits = 0},
    {.baseValue = 10,  .extraBits = 0},
    {.baseValue = 11,  .extraBits = 1},
    {.baseValue = 13,  .extraBits = 1},
    {.baseValue = 15,  .extraBits = 1},
    {.baseValue = 17,  .extraBits = 1},
    {.baseValue = 19,  .extraBits = 2},
    {.baseValue = 23,  .extraBits = 2},
    {.baseValue = 27,  .extraBits = 2},
    {.baseValue = 31,  .extraBits = 2},
    {.baseValue = 35,  .extraBits = 3},
    {.baseValue = 43,  .extraBits = 3},
    {.baseValue = 51,  .extraBits = 3},
    {.baseValue = 59,  .extraBits = 3},
    {.baseValue = 67,  .extraBits = 4},
    {.baseValue = 83,  .extraBits = 4},
    {.baseValue = 99,  .extraBits = 4},
    {.baseValue = 115, .extraBits = 4},
    {.baseValue = 131, .extraBits = 5},
    {.baseValue = 163, .extraBits = 5},
    {.baseValue = 195, .extraBits = 5},
    {.baseValue = 227, .extraBits = 5},
    {.baseValue = 258, .extraBits = 0},
};

static code_lookup distanceLookup[] =
{
    {.baseValue = 1,     .extraBits = 0},
    {.baseValue = 2,     .extraBits = 0},
    {.baseValue = 3,     .extraBits = 0},
    {.baseValue = 4,     .extraBits = 0},
    {.baseValue = 5,     .extraBits = 1},
    {.baseValue = 7,     .extraBits = 1},
    {.baseValue = 9,     .extraBits = 2},
    {.baseValue = 13,    .extraBits = 2},
    {.baseValue = 17,    .extraBits = 3},
    {.baseValue = 25,    .extraBits = 3},
    {.baseValue = 33,    .extraBits = 4},
    {.baseValue = 49,    .extraBits = 4},
    {.baseValue = 65,    .extraBits = 5},
    {.baseValue = 97,    .extraBits = 5},
    {.baseValue = 129,   .extraBits = 6},
    {.baseValue = 193,   .extraBits = 6},
    {.baseValue = 257,   .extraBits = 7},
    {.baseValue = 385,   .extraBits = 7},
    {.baseValue = 513,   .extraBits = 8},
    {.baseValue = 769,   .extraBits = 8},
    {.baseValue = 1025,  .extraBits = 9},
    {.baseValue = 1537,  .extraBits = 9},
    {.baseValue = 2049,  .extraBits = 10},
    {.baseValue = 3073,  .extraBits = 10},
    {.baseValue = 4097,  .extraBits = 11},
    {.baseValue = 6145,  .extraBits = 11},
    {.baseValue = 8193,  .extraBits = 12},
    {.baseValue = 12289, .extraBits = 12},
    {.baseValue = 16385, .extraBits = 13},
    {.baseValue = 24577, .extraBits = 13},
};

static u32 codeLengthMap[] =
{
    16,
    17,
    18,
    0,
    8,
    7,
    9,
    6,
    10,
    5,
    11,
    4,
    12,
    3,
    13,
    2,
    14,
    1,
    15,
};

#if TBS_DEBUG
#define Debug_PrintBitsLSB(text, bits, count) PrintBitsLSB(name, bits, count)
#else
#define Debug_PrintBitsLSB(text, bits, count)
#endif

static void PrintBitsLSB(const char *name, u64 bits, u32 count)
{
    PrintTextF("%s: ", name);
    for(u32 i = count; i > 0; --i)
    {
        PrintTextF("%lld", (bits >> (i-1ull)) & 1ull);
    }
    PrintTextF("\n");
}

static u8 ReverseBitsU8(u8 value)
{
    u8 result = 0;
    result = ((value >> 7) & 1) | result;
    result = ((value >> 5) & 2) | result;
    result = ((value >> 3) & 4) | result;
    result = ((value >> 1) & 8) | result;
    result = ((value << 1) & 16) | result;
    result = ((value << 3) & 32) | result;
    result = ((value << 5) & 64) | result;
    result = ((value << 7) & 128) | result;
    Debug_PrintBitsLSB("Original", value, 8);
    Debug_PrintBitsLSB("Reversed", result, 8);

    return result;
}

static inline u32 ReverseBitsU32(u32 value)
{
    u32 result = 0;
    for(u32 i = 0; i < 31; ++i)
    {
        result |= ((value >> (31 - (2*i))) & (1 << i));
        result |= ((value << (31 - (2*i))) & (1 << (31 - i)));
    }

    Debug_PrintBitsLSB("Reversed value", result, 32);

    return result;
}

static inline u8 EatNextBit(bit_stream *s)
{
    u8 value = *s->offset;
    value = (value >> s->bitOffset) & 1;
    ++s->bitOffset;
    tbs_assert(s->bitOffset <= 8);
    if(s->bitOffset == 8)
    {
        s->bitOffset = 0;
        ++s->offset;
    }

    return value;
}

static u32 EatBitsLSB(bit_stream *s, u32 count)
{
    tbs_assert(count < 32);
    u32 result = 0;
    for(u32 i = 0; i < count; ++i)
    {
        u32 bit = EatNextBit(s);
        result |= (bit << i);
    }

    return result;
}

// TODO(torgrim): Have a flag where the caller can specify if the data
// is in msb or lsb format.
static u32 CalculateCrc32(byte_stream *s)
{
    Debug_StartTimer(CRC32);
    // TODO(torgrim): Currently only using slow version
    // to calculate the crc. Need to also implement a fast version.
    // NOTE(torgrim): msb poly version.
    //u32 pol = 0x04C11DB7;
    // NOTE(torgrim): lsb poly version.
    u32 pol = 0xEDB88320;

    i64 len = s->offset - s->base;
    tbs_assert(len > 0);

    // NOTE(torgrim): Clang on wsl linux doesn't seem to handle this correctly
    // where it thinks i64 == long although the sizeof(i64) == 8. Probably something
    // bad with clang format parsing. Need to test on a different linux machine.
    // Doesn't get this warning with clang on windows.
    PrintDebugF("Calculating CRC for stream with length: %lld \n", (long long)len);

    u32 crc = 0xFFFFFFFF;
    for(u32 i = 0; i < len; ++i)
    {
        // NOTE(torgrim): If the stream is most-significant bit first
        // then we first need to reverse the current byte before processing
        // it.
        // u8 value = ReverseBitsU8(stream[i]);
        u8 value = s->base[i];
        crc = crc ^ value;
        for(u32 j = 0; j < 8; ++j)
        {
            if(crc & 1)
            {
                crc = (crc >> 1) ^ pol;
            }
            else
            {
                crc >>= 1;
            }
        }
    }

    PrintDebugF("\n");
    crc = ~crc;
    Debug_PrintBitsLSB("Result CRC", crc, 32);
    PrintDebugF("CRC Hex: %x\n", crc);

    Debug_EndTimer(CRC32);
    return crc;
}

#if TBS_DEBUG
#define Debug_BitStream_PrintBits(name, s, count) BitStream_PrintBits(name, s, count)
#else
#define Debug_BitStream_PrintBits(name, s, count)
#endif
static void BitStream_PrintBits(const char *name, bit_stream *s, u32 count)
{
    u32 orgBitOffset = s->bitOffset;
    u8 *orgOffset = s->offset;
    PrintTextF("%s: ", name);
    for(u32 i = 0; i < count; ++i)
    {
        u32 bit = EatNextBit(s);
        PrintTextF("%u", bit);
    }

    PrintTextF("\n");

    s->offset = orgOffset;
    s->bitOffset = orgBitOffset;
}

typedef struct mem_pool
{
    u8 *base;
    u32 count;
    u32 capacity;
    size_t elementSize;
} mem_pool;

#define Pool_Create(type, capacity) Pool_Create_Internal(sizeof(type), capacity)
static mem_pool Pool_Create_Internal(size_t elementSize, u32 capacity)
{
    mem_pool result;
    result.base = malloc(elementSize * capacity);
    memset(result.base, 0, elementSize * capacity);
    result.count = 0;
    result.capacity = capacity;
    result.elementSize = elementSize;

    return result;
}

static void *Pool_AllocUnit(mem_pool *p)
{
    void *result = NULL;
    if(p->count < p->capacity)
    {
        result = p->base + (p->count * p->elementSize);
        p->count++;
    }
    else
    {
        PrintTextF("POOL ERROR:: Could not allocate new unit\n");
    }

    tbs_assert(result != NULL);
    return result;
}

static huff_tree_node *CodeLenCodes_CreateHuffmanTree(bit_stream *s, u32 readCount, mem_pool *nodePool)
{
    PrintDebugF("Code Length Count: %u\n", readCount);
    u32 codeLengthFreq[8] = {0};
    u32 codeLengths[ARRAY_COUNT(codeLengthMap)] = {0};
    for(u32 i = 0; i < readCount; ++i)
    {
        u32 trueIndex = codeLengthMap[i];
        u32 codeLength = EatBitsLSB(s, 3);
        tbs_assert(codeLength < ARRAY_COUNT(codeLengthMap));
        codeLengths[trueIndex] = codeLength;
        PrintDebugF("Value: %u, Length: %u\n", codeLengthMap[i], codeLengths[trueIndex]);
        codeLengthFreq[codeLength]++;
    }

    codeLengthFreq[0] = 0;
    u32 nextValue[8] = {0};
    u32 prevBaseValue = 0;
    u32 prevCount = 0;
    for(u32 i = 1;i <= 7; ++i)
    {
        PrintDebugF("Length: %u, Count: %u\n", i, codeLengthFreq[i]);
        u32 baseValue = (prevBaseValue + prevCount) << 1;
        prevBaseValue = baseValue;
        prevCount = codeLengthFreq[i];
        nextValue[i] = baseValue;
    }

    for(u32 i = 1; i <= 7; ++i)
    {
        PrintDebugF("Bit Length: %u, Base: %u\n", i, nextValue[i]);
    }

    u32 codes[19] = {0};

    for(u32 i = 0; i < 19; ++i)
    {
        u32 cl = codeLengths[i];
        if(cl > 0)
        {
            // TODO(torgrim): Not sure I like that this is changing
            // the input list. If this should be the case, make it more explicit to the caller
            // that the list is modified.
            codes[i] = nextValue[cl];
            nextValue[cl]++;
        }
    }

    huff_tree_node *root = Pool_AllocUnit(nodePool);
    for(u32 i = 0; i < 19; ++i)
    {
        u32 len = codeLengths[i];
        u32 code = codes[i];
        PrintDebugF("Value: %u, Length: %u, ", i, len);
        Debug_PrintBitsLSB("Code", code, len);

        if(len > 0)
        {
            huff_tree_node *prevNode = root;
            for(u32 j = 0; j < len-1u; ++j)
            {
                u32 childIndex = (code >> ((len - 1) - j)) & 1;
                if(prevNode->children[childIndex] == NULL)
                {
                    huff_tree_node *newNode = Pool_AllocUnit(nodePool);
                    newNode->type = NodeType_Bit;
                    prevNode->children[childIndex] = newNode;
                }

                prevNode = prevNode->children[childIndex];
            }


            u32 leafIndex = code & 1;
            huff_tree_node *leafNode = Pool_AllocUnit(nodePool);

            leafNode->type = NodeType_Leaf;
            leafNode->value = i;

            tbs_assert(prevNode->children[leafIndex] == NULL);
            prevNode->children[leafIndex] = leafNode;
        }
    }

    return root;
}

static huff_tree_node *CreateHuffCodesLitLenDist(u32 *list, u32 count, mem_pool *nodePool)
{
    u32 codeLengthFreq[16] = {0};
    for(u32 i = 0; i < count; ++i)
    {
        tbs_assert(list[i] < 16);
        codeLengthFreq[list[i]]++;
    }

    codeLengthFreq[0] = 0;

    u32 prevBaseValue = 0;
    u32 prevCount = 0;
    u32 nextValue[16] = {0};
    for(u32 i = 1; i <= 15; ++i)
    {
        PrintDebugF("Length: %u, Count: %u\n", i, codeLengthFreq[i]);
        u32 baseValue = (prevBaseValue + prevCount) << 1;
        prevBaseValue = baseValue;
        prevCount = codeLengthFreq[i];
        nextValue[i] = baseValue;
    }

    huff_tree_node *root = Pool_AllocUnit(nodePool);
    for(u32 i = 0; i < count; ++i)
    {
        u32 len = list[i];
        u32 code = nextValue[len];
        nextValue[len]++;
        if(len > 0)
        {
            tbs_assert(codeLengthFreq[len] > 0);
            PrintDebugF("Index %u, ", i);
            Debug_PrintBitsLSB("Code", code, len);

            huff_tree_node *prevNode = root;
            for(u32 j = 0; j < len-1; ++j)
            {
                u32 childIndex = (code >> ((len - 1) - j)) & 1;
                if(prevNode->children[childIndex] == NULL)
                {
                    huff_tree_node *newNode = Pool_AllocUnit(nodePool);
                    newNode->type = NodeType_Bit;
                    prevNode->children[childIndex] = newNode;
                }

                prevNode = prevNode->children[childIndex];
            }


            u32 leafIndex = code & 1;
            huff_tree_node *leafNode = Pool_AllocUnit(nodePool);

            leafNode->type = NodeType_Leaf;
            leafNode->value = i;

            tbs_assert(prevNode->children[leafIndex] == NULL);
            prevNode->children[leafIndex] = leafNode;
        }
    }

    return root;
}

static void CodeLenCodes_DecodeHuffmanStream(bit_stream *s, huff_tree_node *rootNode, u32 count, u32 *result)
{
    huff_tree_node *node = rootNode;
    u32 resultIndex = 0;
    u32 dataIndex = 0;
    while(dataIndex < count)
    {
        PrintDebugF("Bits: ");
        while(node != NULL && node->type != NodeType_Leaf)
        {
            u8 nextBit = EatNextBit(s);
            PrintDebugF("%u", nextBit);
            node = node->children[nextBit];
        }

            PrintDebugF(" ");
            tbs_assert(node != NULL);
            if(node->type == NodeType_Leaf)
            {
                if(node->value < 16)
                {
                    PrintDebugF("Got Code Length < 16\n");
                    PrintDebugF("Data Index: %u, Value: %u\n", dataIndex, node->value);
                    ++dataIndex;
                    result[resultIndex++] = node->value;
                }
                else if(node->value == 16)
                {
                    PrintDebugF("====Got Code Length = 16 ====\n");
                    u32 length = EatBitsLSB(s, 2) + 3;
                    PrintDebugF("Repeat length: %u\n", length);
                    tbs_assert(resultIndex > 0);

                    // TODO(torgrim): Just use memset?
                    u32 value = result[resultIndex-1];
                    for(u32 i = 0; i < length; ++i)
                    {
                        result[resultIndex++] = value;
                    }
                    dataIndex += length;
                }
                else if(node->value == 17)
                {
                    PrintDebugF("Got Code Length = 17\n");
                    u32 length = EatBitsLSB(s, 3) + 3;
                    PrintDebugF("Repeat Length = %u\n", length);
                    // TODO(torgrim): Just use memset?
                    for(u32 i = 0; i < length; ++i)
                    {
                        result[resultIndex++] = 0;
                    }
                    dataIndex += length;
                }
                else if(node->value == 18)
                {
                    PrintDebugF("Got Code Length = 18\n");
                    u32 length = EatBitsLSB(s, 7) + 11;
                    PrintDebugF("Repeat Length = %u\n", length);

                    // TODO(torgrim): Just use memset?
                    for(u32 i = 0; i < length; ++i)
                    {
                        result[resultIndex++] = 0;
                    }
                    dataIndex += length;
                }
                else
                {
                    tbs_assert(false);
                }
            }
            else
            {
                tbs_assert(false);
            }

            node = rootNode;
    }

    //CreateHuffmanTree(litLenAlphabet, litCodeLengthCount);
    PrintDebugF("Index on end: %u\n", resultIndex);
    PrintDebugF("Should be below or equal: %u\n", count);
    tbs_assert(resultIndex <= count);
}

static void DecompressDynamicHuffman(bit_stream *inStream, byte_stream *outStream)
{
    Debug_StartTimer(DynamicHuffman);
    u32 HLIT = EatBitsLSB(inStream, 5);
    u32 HDIST = EatBitsLSB(inStream, 5);
    u32 HCLEN = EatBitsLSB(inStream, 4);

    PrintDebugF("HLIT: %u\n", HLIT);
    PrintDebugF("HDIST: %u\n", HDIST);
    PrintDebugF("HCLEN: %u\n", HCLEN);

    u32 actualCodeCount = HCLEN+4;
    mem_pool codeLenCodesNodePool = Pool_Create(huff_tree_node, (1 << 8) - 1);
    tbs_assert(actualCodeCount <= ARRAY_COUNT(codeLengthMap));
    huff_tree_node *root = CodeLenCodes_CreateHuffmanTree(inStream, actualCodeCount, &codeLenCodesNodePool);
    PrintDebugF("\n\nProcessing Lit/length codes...\n\n");

    u32 litCodeLengthCount = HLIT + 257;
    u32 *litLengths = malloc(sizeof(u32) * litCodeLengthCount);
    CodeLenCodes_DecodeHuffmanStream(inStream, root, litCodeLengthCount, litLengths);
    PrintDebugF("\n\nProcessing Distance Codes...\n\n");
    u32 distCodeLengthCount = HDIST + 1;
    u32 *distLengths = malloc(sizeof(u32) * distCodeLengthCount);
    CodeLenCodes_DecodeHuffmanStream(inStream, root, distCodeLengthCount, distLengths);

    mem_pool litDistLenPool = Pool_Create(huff_tree_node, ((1 << 16) - 1) + ((1 << 16) - 1));
    PrintDebugF("\n\nCreating LitLen Huffman Codes\n\n");
    huff_tree_node *litLenRoot = CreateHuffCodesLitLenDist(litLengths, litCodeLengthCount, &litDistLenPool);
    PrintDebugF("\n\nCreating Distance Huffman Codes\n\n");
    huff_tree_node *distRoot = CreateHuffCodesLitLenDist(distLengths, distCodeLengthCount, &litDistLenPool);

    PrintDebugF("\n\nDecoding Compressed Data...\n\n");

    while(true)
    {
        huff_tree_node *node = litLenRoot;
        while(node != NULL && node->type != NodeType_Leaf)
        {
            u8 nextBit = EatNextBit(inStream);
            PrintDebugF("%u", nextBit);
            node = node->children[nextBit];
        }

        tbs_assert(node != NULL);
        if(node->value < 256)
        {
            PrintDebugF(", Lit Value: %u\n", node->value);
            *outStream->offset = (u8)node->value;
            ++outStream->offset;
        }
        else if(node->value > 256)
        {
            u32 baseValue = lengthLookup[node->value-257].baseValue;
            u32 offsetValue = 0;
            u32 len = 0;
            for(u32 ei = 0; ei < lengthLookup[node->value-257].extraBits; ++ei)
            {
                u32 bit = EatNextBit(inStream);
                offsetValue |= (bit << ei);
            }

            PrintDebugF(", Len Value: %u, Len: %u, ", node->value, baseValue + offsetValue);
            len = baseValue + offsetValue;
            node = distRoot;
            while(node != NULL && node->type != NodeType_Leaf)
            {
                u8 nextBit = EatNextBit(inStream);
                PrintDebugF("%u", nextBit);
                node = node->children[nextBit];
            }

            tbs_assert(node != NULL);
            tbs_assert(node->value < 32);
            baseValue = distanceLookup[node->value].baseValue;
            offsetValue = 0;
            for(u32 ei = 0; ei < distanceLookup[node->value].extraBits; ++ei)
            {
                u32 bit = EatNextBit(inStream);
                offsetValue |= (bit << ei);
            }
            PrintDebugF(", Dist Index: %u, Dist: %u\n", node->value, baseValue + offsetValue);

            u32 dis = baseValue + offsetValue;
            for(u32 i = len; i > 0; --i)
            {
                *outStream->offset = *(outStream->offset - dis);
                ++outStream->offset;
            }
        }
        else
        {
            PrintDebugF("\n\nFound End Of Compressed stream\n\n");
            break;
        }
    }
    PrintDebugF("\n");

    Debug_EndTimer(DynamicHuffman);
}

static void DecompressFixedHuffman(bit_stream *inStream, byte_stream *outStream)
{
    Debug_StartTimer(FixedHuffman);
    while(true)
    {
        u32 code = 0;

        tbs_assert(inStream->bitOffset < 8);
        u32 bi = inStream->bitOffset;
        u16 bits = *((u16 *)inStream->offset);
        bits >>= bi;

        code = 0;
        code = (bits & 1u);
        for(u32 i = 1; i < 9; ++i)
        {
            code = ((bits >> i) & 1u) | (code << 1u);
        }

        u32 litLo = code >> 1;
        u32 litHi = code;
        u32 lenLo = code >> 2;
        u32 lenHi = litLo;
        bool parseDist = false;
        u32 value = 0;
        if(litLo >= 48 && litLo <= 191)
        {
            PrintDebugF("Low literal value code found\n");
            value = litLo - 48;
            code = litLo;
            PrintDebugF("Actual Value: %u\n", value);
            bi += 8;
            *outStream->offset = (u8)value;
            ++outStream->offset;
        }
        else if(litHi >= 400 && litHi <= 511)
        {
            PrintDebugF("Hight literal value code found\n");
            value = 144 + (litHi - 400);
            code = litHi;
            bi += 9;
            *outStream->offset = (u8)value;
            ++outStream->offset;
        }
        else if(lenLo <= 23u)
        {
            PrintDebugF("Low length value code found\n");
            code = lenLo;
            bi += 7;
            parseDist = true;
            // NOTE(torgrim): Since this code starts at 256 but our
            // code lookup is only for code >= 257 we need to offset it by one.
            value = 256 + lenLo;
        }
        else if(lenHi >= 192 && lenHi <= 199)
        {
            PrintDebugF("High length value code found\n");
            code = lenHi;
            bi += 8;
            parseDist = true;
            value = 280 + (lenHi - 192);
        }
        else
        {
            PrintDebugF("FIXED HUFFMAN::ERROR:: Invalid Huffman code found\n");
        }

        if(bi >= 8)
        {
            inStream->offset += (bi / 8);
            bi %= 8;
        }

        Debug_PrintBitsLSB("Code", code, 16);
        inStream->bitOffset = bi;

        if(value == 256)
        {
            break;
        }

        if(parseDist && code != 0)
        {
            // TODO(torgrim): Does lenCode = 256(stream end) actually have a dist value
            // or is it just the len value?

            u32 len = 0;
            u32 dis = 0;
            tbs_assert(value >= 257);
            if((value - 257) >= ARRAY_COUNT(lengthLookup))
            {
                PrintDebugF("Length Value: %u\n", value);
                tbs_assert(false);
            }
            code_lookup lenInfo = lengthLookup[value - 257];
            PrintDebugF("Length Base: %u, Extra Bits: %u\n", lenInfo.baseValue, lenInfo.extraBits);
            len = lenInfo.baseValue;

            u32 offsetValue = 0;
            for(u32 ei = 0; ei < lenInfo.extraBits; ++ei)
            {
                u32 bit = EatNextBit(inStream);
                offsetValue |= (bit << ei);
            }

            len += offsetValue;
            PrintDebugF("Actual Length: %u\n", len);

            u8 distCode = 0;
            for(u32 i = 0; i < 5; ++i)
            {
                u32 bit = EatNextBit(inStream);
                distCode |= (bit << (4-i));
            }

            PrintDebugF("Distance Code: %hhu\n", distCode);
            tbs_assert(distCode < ARRAY_COUNT(distanceLookup));
            code_lookup disInfo = distanceLookup[distCode];
            PrintDebugF("Distance Base: %u, Extra Bits: %u\n", disInfo.baseValue, disInfo.extraBits);

            dis = disInfo.baseValue;
            offsetValue = 0;
            for(u32 ei = 0; ei < disInfo.extraBits; ++ei)
            {
                u32 bit = EatNextBit(inStream);
                offsetValue |= (bit << ei);
            }

            dis += offsetValue;
            for(u32 i = len; i > 0; --i)
            {
                *outStream->offset = *(outStream->offset - dis);
                ++outStream->offset;
            }
        }
    }

    Debug_EndTimer(FixedHuffman);
}

static void ProcessNonCompressedBlock(bit_stream *inStream, byte_stream *outStream)
{
    Debug_StartTimer(NonCompressed);
    u16 len = *(u16 *)inStream->offset;
    inStream->offset += sizeof(u16);
    u16 nlen = *(u16 *)inStream->offset;
    inStream->offset += sizeof(nlen);
    PrintDebugF("Non-compressed Block Length: %u\n", len);
    memcpy(outStream->offset, inStream->offset, len);
    inStream->offset += len;
    outStream->offset += len;
    Debug_EndTimer(NonCompressed);
}

static inline u32 StringLengthZ(char *s)
{
    u32 result = 0;
    while(*s++ != '\0')
    {
        ++result;
    }

    return result;
}

static inline bool StringMatchSubZ(char *s, char *sub)
{
    bool result = true;
    u32 len1 = StringLengthZ(s);
    u32 len2 = StringLengthZ(sub);

    if(len1 >= len2)
    {
        for(u32 i = 0; i < len2; ++i)
        {
            if(s[i] != sub[i])
            {
                result = false;
                break;
            }
        }
    }

    return result;
}

#if defined(_WIN64)
static char *GetCWD(void)
{
    char *result = NULL;
    DWORD neededSize = GetCurrentDirectory(0, NULL);
    tbs_assert(neededSize > 0);

    char *buffer = malloc(sizeof(char) * neededSize);
    if(GetCurrentDirectory(neededSize, buffer))
    {
        result = buffer;
    }
    return result;
}

static file_data ReadAllFile(char *filename)
{
    file_data result = {0};
    HANDLE fHandle = CreateFileA(filename, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);
    if(fHandle != INVALID_HANDLE_VALUE)
    {
        LARGE_INTEGER fileSize;
        if(GetFileSizeEx(fHandle, &fileSize))
        {
            if(fileSize.LowPart > 0)
            {
                u8 *buffer = malloc(sizeof(u8) * fileSize.LowPart);
                DWORD bytesRead = 0;
                if(ReadFile(fHandle, buffer, fileSize.LowPart, &bytesRead, NULL))
                {
                    result.data = buffer;
                    result.size = fileSize.LowPart;
                }
                else
                {
                    PrintTextF("FILE_IO_ERROR:: Could not read file\n");
                }
            }
        }
        else
        {
            PrintTextF("FILE_IO_ERROR:: Could not get file size\n");
        }

        CloseHandle(fHandle);
    }
    else
    {
        PrintTextF("FILE_IO_ERROR:: Could not create file\n");
    }

    return result;
}

static bool WriteAllFile(char *filename, u8 *data, u32 size)
{
    bool result = false;
    HANDLE fHandle = CreateFileA(filename, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL, NULL);
    if(fHandle != INVALID_HANDLE_VALUE)
    {
        DWORD bytesWritten = 0;
        result = WriteFile(fHandle, data, size, &bytesWritten, NULL);
        tbs_assert(bytesWritten == size);

        CloseHandle(fHandle);
    }

    return result;
}
#elif defined(__linux__)
static char *GetCWD(void)
{
    char *result = getcwd(NULL, 0);
    tbs_assert(result != NULL);
    return result;
}

static file_data ReadAllFile(char *filename)
{
    file_data result = {0};
    int openFlags = O_RDONLY;
    int fHandle = open(filename, openFlags);
    if(fHandle != -1)
    {
        file_stat fInfo;
        int succ = fstat(fHandle, &fInfo);
        if(succ != -1)
        {
            u8 *buffer = malloc(sizeof(u8) * (size_t)fInfo.st_size);
            ssize_t readSize = read(fHandle, buffer, (size_t)fInfo.st_size);
            if(readSize > -1 && readSize == fInfo.st_size)
            {
                result.data = buffer;
                result.size = (u64)readSize;
            }
            else
            {
                PrintTextF("FILE_IO_ERROR:: Could not read file\n");
            }
        }

        close(fHandle);
    }
    else
    {
        PrintTextF("FILE_IO_ERROR:: Could not create file\n");
    }

    return result;
}

static bool WriteAllFile(char *filename, u8 *data, u64 size)
{
    bool result = false;
    int fHandle = open(filename, O_WRONLY|O_CREAT|O_TRUNC, S_IRUSR|S_IWUSR);
    if(fHandle > -1)
    {
        ssize_t bytesWritten = write(fHandle, data, size);
        if(bytesWritten == (ssize_t)size)
        {
            result = true;
        }

        close(fHandle);
    }

    return result;
}

#endif

static char *DirFileConcat(char *s1, u32 len1,
                           char *s2, u32 len2)
{
    tbs_assert(len1 > 0 || len2 > 0);
    // NOTE(torgrim): +2 for null char and / separator
    size_t bufferSize = sizeof(char) * (len1+len2+2);
    char *result = malloc(bufferSize);
    snprintf(result, bufferSize, "%.*s/%.*s", len1, s1, len2, s2);

    return result;
}

int main(int argc, char *argv[])
{
    Debug_StartTimer(GlobalApplication);
    char *cwd = GetCWD();
    u32 cwdLen = StringLengthZ(cwd);
    PrintTextF("Current Working Directory: %s\n", cwd);
    PrintDebugF("CWD string length: %u\n", cwdLen);

    char *outputFilename = NULL;
    char *inputFilename = NULL;
    for(i32 i = 1; i < argc; ++i)
    {
        char *arg = argv[i];
        if(StringMatchSubZ(arg, "-o"))
        {
            char *optArg = arg + 2;
            u32 len = StringLengthZ(optArg);
            if(len > 0)
            {
                outputFilename = DirFileConcat(cwd, cwdLen, optArg, len);
            }
        }
        else if(StringMatchSubZ(arg, "-i"))
        {
            char *optArg = arg + 2;
            u32 len = StringLengthZ(optArg);
            if(len > 0)
            {
                inputFilename = DirFileConcat(cwd, cwdLen, optArg, len);
            }
        }
    }

    if(inputFilename == NULL || outputFilename == NULL)
    {
        if(inputFilename == NULL)
        {
            PrintTextF("\nError: No input file provided");
        }
        else
        {
            PrintTextF("\nError: No output file provided");
        }
        PrintTextF("\nUsage: gzip_decoder.exe -i[input_filename] -o[output_filename]");
        PrintTextF("\nBoth input_filename and output_filename needs to be provided(order doesn't matter)\n");
        return 0;
    }
    tbs_assert(inputFilename != NULL);
    tbs_assert(outputFilename != NULL);

    PrintTextF("Input File: %s\n", inputFilename);
    PrintTextF("Output File: %s\n", outputFilename);

    file_data inputFile = ReadAllFile(inputFilename);
    if(inputFile.size > 0)
    {
        PrintTextF("File opened successfully\n");
        byte_stream stream = {0};
        stream.base = inputFile.data;
        stream.size = inputFile.size;
        if(stream.size > sizeof(gzip_header))
        {
            stream.offset = stream.base;

            tbs_static_assert(sizeof(gzip_header) == 10, "Incorrect structure size");
            gzip_header header = *(gzip_header *)stream.offset;
            stream.offset += sizeof(gzip_header);

            // TODO(torgrim): Do a check here that we got the
            // gzip signature.

            PrintTextF("\n==================== GZIP Header ====================\n");
            PrintTextF("ID1: %x\n", header.ID1);
            PrintTextF("ID2: %x\n", header.ID2);
            PrintTextF("CM: %hhu\n", header.CM);
            PrintBitsLSB("FLG", header.FLG, 8);
            PrintBitsLSB("XFL", header.XFL, 8);
            PrintBitsLSB("OS", header.OS, 8);

            tbs_assert(header.XFL == 0);

            if(header.FLG & GzipFlags_FNAME)
            {
                u8 *nameStart = stream.offset;
                while(*stream.offset != '\0')
                {
                    ++stream.offset;
                }

                ++stream.offset;

                PrintTextF("Original file name: %s\n", nameStart);
            }
            else
            {
                tbs_assert(header.FLG == 0);
            }

            u32 CRC32 = *(u32 *)((stream.base + (stream.size - 8)));
            u32 ISIZE = *(u32 *)((stream.base + (stream.size - 4)));
            PrintTextF("CRC: %x\n", CRC32);
            PrintTextF("ISIZE: %u bytes\n", ISIZE);

            PrintTextF("==================== GZIP Header ====================\n");

            byte_stream resultStream = {0};
            resultStream.base = malloc(sizeof(u8) * ISIZE);
            resultStream.offset = resultStream.base;
            resultStream.size = 0;
            bit_stream compressStream = {.base = stream.offset, .offset = stream.offset, .bitOffset = 0, .size = 0};
            u8 BFINAL = 0;
            //deflate_header h = {0};
            do
            {
                PrintDebugF("====Block Start====\n");
                // TODO(torgrim): This doesn't need to be its own function.
                BFINAL = EatNextBit(&compressStream);
                u8 BTYPE = (u8)EatBitsLSB(&compressStream, 2);
                if(BFINAL)
                {
                    PrintDebugF("!!!!Is Final Block!!!!\n");
                }
                Debug_PrintBitsLSB("BFINAL", BFINAL, 8);
                Debug_PrintBitsLSB("BTYPE", BTYPE, 8);
                switch(BTYPE)
                {
                    case BlockType_NoCompress:
                        {
                            PrintDebugF("BlockType No Compression\n");
                            if(compressStream.bitOffset > 0)
                            {
                                compressStream.bitOffset = 0;
                                ++compressStream.offset;
                            }
                            ProcessNonCompressedBlock(&compressStream, &resultStream);
                        }break;
                    case BlockType_Fixed:
                        {
                            PrintDebugF("BlockType Fixed Huffman\n");
                            u16 code = 0;
                            do
                            {
                                DecompressFixedHuffman(&compressStream, &resultStream);
                            } while(code != 0);
                        }break;
                    case BlockType_Dynamic:
                        {
                            DecompressDynamicHuffman(&compressStream, &resultStream);
                            PrintDebugF("BlockType Dynamic Huffman\n");
                        }break;
                    default:
                        {
                            PrintDebugF("HEADER_ERROR::Unknown BlockType\n");
                            PrintDebugF("Current Output Size: %zu\n", resultStream.offset - resultStream.base);
                            tbs_assert(false);
                        }
                }
            } while(!BFINAL);

            tbs_assert(resultStream.offset >= resultStream.base);
            ptrdiff_t resultSize = resultStream.offset - resultStream.base;
            tbs_assert(resultSize < UINT_MAX);

            PrintDebugF("Result ASCII: %.*s\n", (i32)(resultSize), resultStream.base);

            WriteAllFile(outputFilename, resultStream.base, (u32)resultSize);

#if 0
            printf("Result Bits: ");
            for(u32 i = 0; i < resultSize; ++i)
            {
                u8 value = *(resultStream.base + i);
                for(u32 i = 8; i > 0; --i)
                {
                    printf("%u", (value >> (i-1)) & 1u);
                }
            }
            printf("\n");
#endif

            compressStream.offset += 1;
            compressStream.bitOffset = 0;
            // TODO(torgrim): Assert that these are always equal to
            // the CRC and ISIZE we calculated previous.
            PrintTextF("\n==================== CRC ====================\n");
            Debug_BitStream_PrintBits("CRC bits", &compressStream, 32);
            compressStream.offset += 4;
            Debug_BitStream_PrintBits("ISIZE bits", &compressStream, 32);
            u32 resultCRC = CalculateCrc32(&resultStream);
            PrintTextF("Results CRC: %x\n", resultCRC);
            PrintTextF("File CRC: %x\n", CRC32);
            PrintTextF("Original File Size: %u bytes\n", ISIZE);
            PrintTextF("Output Stream Size: %zu bytes\n", resultSize);
            if(resultCRC == CRC32)
            {
                PrintTextF("!!!! Found Valid CRC !!!!\n");
            }
            else
            {
                PrintTextF("!!!! Invalid CRC Found !!!!\n");
            }
            PrintTextF("==================== CRC ====================\n");
        }
    }
    else
    {
        PrintTextF("FILE ERROR:: Could not find input file\n");
    }

    Debug_EndTimer(GlobalApplication);

    PrintTextF("\n==================== Decompress Timing Details (Microseconds) ====================\n");
    PrintTextF("Elapsed Time: %lld\n", (long long)Debug_timerList[DebugTimerType_GlobalApplication]);
    PrintTextF("Elapsed Time: %lld\n", (long long)Debug_timerList[DebugTimerType_FixedHuffman]);
    PrintTextF("Elapsed Time: %lld\n", (long long)Debug_timerList[DebugTimerType_DynamicHuffman]);
    PrintTextF("Elapsed Time: %lld\n", (long long)Debug_timerList[DebugTimerType_NonCompressed]);
    PrintTextF("Elapsed Time: %lld\n", (long long)Debug_timerList[DebugTimerType_CRC32]);
    PrintTextF("==================== Decompress Timing Details ====================\n");

    return 0;
}
