@echo off

set code_dir=%~dp0
set build_dir="..\build\"

set tbs_debug_def=-DTBS_DEBUG=0

rem extra padding added warning
set msvc_disable_err=%msvc_disable_err% /wd4820

rem nonstandard extension used(mostly for designated initializers) which aren't supported
rem in msvc yet.
set msvc_disable_err=%msvc_disable_err% /wd4204

rem Spectre mitigation if /Qspectre switch specified
set msvc_disable_err=%msvc_disable_err% /wd5045

rem Function selected for automatic inline expansion
set msvc_disable_err=%msvc_disable_err% /wd4711

rem Function not inlined
set msvc_disable_err=%msvc_disable_err% /wd4710

rem unused function
set clang_disable_err=-Wno-unused-function

rem macro with variadic arguments require all arguments.
rem This happens because we want a printf-like function to compile
rem into nothing.
rem MSVC doesn't seem to complain about this.
set clang_disable_err=%clang_disable_err% -Wno-gnu-zero-variadic-macro-arguments

pushd %code_dir%

if not exist %build_dir% mkdir %build_dir%

pushd %build_dir%

rem clang -O0 -Weverything -Werror -o gzip_decoder.exe %clang_disable_err% %tbs_debug_def% %code_dir%gzip_decoder.c
cl /nologo /Od /Zi /TC /Wall /WX %msvc_disable_err% %tbs_debug_def% %code_dir%gzip_decoder.c

popd
popd
