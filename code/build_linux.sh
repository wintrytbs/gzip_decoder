#!/bin/bash

if [ ! -d "../build" ]; then
    mkdir "../build"
fi

tbs_debug_def=-DTBS_DEBUG=0
clang_disabled_err="-Wno-gnu-zero-variadic-macro-arguments"
clang_disabled_err="$clang_disabled_err -Wno-padded"

# Can be good to enable from time to time to check if there are any
# functions that we actually want to remove?
clang_disabled_err="$clang_disabled_err -Wno-unused-function"

# Should fix these so that we also access items aligned
# but ok for now.
clang_disabled_err="$clang_disabled_err -Wno-cast-align"

pushd "../build" > /dev/null

clang -Weverything -Werror -std=c11 $clang_disabled_err $tbs_debug_def -o gzip_decoder ../code/gzip_decoder.c

popd > /dev/null
